import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TableComponent } from './app-table/table.component';
import { SelectCustomerComponent } from './app-selects/select-customer/select-customer.component';
import { SelectBillerComponent } from './app-selects/select-biller/select-biller.component';
import { CustomerPageComponent } from './customer-page/customer-page.component';
import { BillersPageComponent } from './billers-page/billers-page.component';
// import { CustomerDetailComponent } from './customer-page/customer-detail/customer-detail.component';
import { BillersDetailComponent } from './billers-page/billers-detail/billers-detail.component';
import { PaymentsPageComponent } from './payments-page/payments-page.component';
import { PaymentsDetailComponent } from './payments-page/payments-detail/payments-detail.component';
import { DetailButtonComponent } from './payments-page/detail-button/detail-button.component';
import { PayButtonComponent } from './payments-page/payments-detail/pay-button/pay-button.component';
import { DetailCustomerComponent } from './customer-page/detail-customer/detail-customer.component';
import { DetailCustomerAddComponent } from './customer-page/detail-customer-add/detail-customer-add.component';
import { DetailCustomerDeleteComponent } from './customer-page/detail-customer-delete/detail-customer-delete.component';
import { DetailCustomerUpdateComponent } from './customer-page/detail-customer-update/detail-customer-update.component';
import { DetailBillerAddComponent } from './billers-page/detail-biller-add/detail-biller-add.component';
import { DetailBillerUpdateComponent } from './billers-page/detail-biller-update/detail-biller-update.component';
import { DetailBillerDeleteComponent } from './billers-page/detail-biller-delete/detail-biller-delete.component';
import { BtnSubmitComponent } from './btn-submit/btn-submit.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.


    // HttpClientInMemoryWebApiModule.forRoot(
    //   InMemoryDataService, { dataEncapsulation: false }
    // )
  ],
  declarations: [
    AppComponent,
    PaymentsPageComponent,
    PaymentsDetailComponent,
    TableComponent,
    SelectCustomerComponent,
    SelectBillerComponent,
    CustomerPageComponent,
    BillersPageComponent,
    BillersDetailComponent,
    DetailButtonComponent,
    PayButtonComponent,
    DetailCustomerComponent,
    DetailCustomerAddComponent,
    DetailCustomerDeleteComponent,
    DetailCustomerUpdateComponent,
    DetailBillerAddComponent,
    DetailBillerUpdateComponent,
    DetailBillerDeleteComponent,
    BtnSubmitComponent
  ],
  providers: [

  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
