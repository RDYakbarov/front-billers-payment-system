export class Payment {
  id: number;
  name: string;
  customer: string;
  biller: string;
  account: number;
  amount: string;
}
