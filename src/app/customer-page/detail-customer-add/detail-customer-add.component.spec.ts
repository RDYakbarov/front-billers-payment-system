import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCustomerAddComponent } from './detail-customer-add.component';

describe('DetailCustomerAddComponent', () => {
  let component: DetailCustomerAddComponent;
  let fixture: ComponentFixture<DetailCustomerAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCustomerAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCustomerAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
