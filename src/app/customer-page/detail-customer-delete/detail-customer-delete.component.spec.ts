import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCustomerDeleteComponent } from './detail-customer-delete.component';

describe('DetailCustomerDeleteComponent', () => {
  let component: DetailCustomerDeleteComponent;
  let fixture: ComponentFixture<DetailCustomerDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCustomerDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCustomerDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
