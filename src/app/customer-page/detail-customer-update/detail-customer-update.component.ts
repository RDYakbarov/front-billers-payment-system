import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-detail-customer-update',
  templateUrl: './detail-customer-update.component.html',
  styleUrls: ['./detail-customer-update.component.css']
})
export class DetailCustomerUpdateComponent implements OnInit {
  customers: string[] = [
    'test',
    'test',
    'test',
    'test',
    'test',
  ];
  billers: string[] = [
    'test',
    'test',
    'test',
    'test',
    'test',
  ];
  myform: FormGroup;

  ngOnInit() {
    this.myform = new FormGroup({
      customer: new FormControl(),
      biller: new FormControl(),
      amount: new FormControl(),
      account: new FormControl()
    });
  }

}
