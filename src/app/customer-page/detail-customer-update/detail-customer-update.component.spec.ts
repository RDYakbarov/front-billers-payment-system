import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCustomerUpdateComponent } from './detail-customer-update.component';

describe('DetailCustomerUpdateComponent', () => {
  let component: DetailCustomerUpdateComponent;
  let fixture: ComponentFixture<DetailCustomerUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCustomerUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCustomerUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
