import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PaymentsPageComponent } from './payments-page/payments-page.component';
import { PaymentsDetailComponent } from './payments-page/payments-detail/payments-detail.component';
import { CustomerPageComponent } from './customer-page/customer-page.component';
import { BillersPageComponent } from './billers-page/billers-page.component';
import { DetailCustomerAddComponent } from './customer-page/detail-customer-add/detail-customer-add.component';
import { DetailCustomerDeleteComponent } from './customer-page/detail-customer-delete/detail-customer-delete.component';
import { DetailCustomerUpdateComponent } from './customer-page/detail-customer-update/detail-customer-update.component';
import { DetailBillerUpdateComponent } from './billers-page/detail-biller-update/detail-biller-update.component';
import { DetailBillerAddComponent } from './billers-page/detail-biller-add/detail-biller-add.component';
import { DetailBillerDeleteComponent } from './billers-page/detail-biller-delete/detail-biller-delete.component';

const routes: Routes = [
  { path: '', redirectTo: '/payments', pathMatch: 'full' },
  { path: 'payments', component: PaymentsPageComponent},
  { path: 'customers', component: CustomerPageComponent},
  { path: 'billers', component: BillersPageComponent},
  { path: 'payments-detail', component: PaymentsDetailComponent},
  { path: 'add-customer', component: DetailCustomerAddComponent},
  { path: 'update-customer', component: DetailCustomerUpdateComponent},
  { path: 'delete-customer', component: DetailCustomerDeleteComponent},
  { path: 'add-biller', component: DetailBillerAddComponent},
  { path: 'update-biller', component: DetailBillerUpdateComponent},
  { path: 'delete-biller', component: DetailBillerDeleteComponent},
  // { path: 'detail/:id', component: UserDetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
