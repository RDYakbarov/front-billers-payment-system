import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillersDetailComponent } from './billers-detail.component';

describe('BillersDetailComponent', () => {
  let component: BillersDetailComponent;
  let fixture: ComponentFixture<BillersDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
