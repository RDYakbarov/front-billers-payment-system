import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-detail-biller-add',
  templateUrl: './detail-biller-add.component.html',
  styleUrls: ['./detail-biller-add.component.css']
})
export class DetailBillerAddComponent implements OnInit {

  myform: FormGroup;

  ngOnInit() {
    this.myform = new FormGroup({
      customer: new FormControl(),
      biller: new FormControl(),
      amount: new FormControl(),
      account: new FormControl()
    });
  }

}
