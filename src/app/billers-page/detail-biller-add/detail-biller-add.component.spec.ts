import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailBillerAddComponent } from './detail-biller-add.component';

describe('DetailBillerAddComponent', () => {
  let component: DetailBillerAddComponent;
  let fixture: ComponentFixture<DetailBillerAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailBillerAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailBillerAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
