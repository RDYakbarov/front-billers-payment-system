import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailBillerDeleteComponent } from './detail-biller-delete.component';

describe('DetailBillerDeleteComponent', () => {
  let component: DetailBillerDeleteComponent;
  let fixture: ComponentFixture<DetailBillerDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailBillerDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailBillerDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
