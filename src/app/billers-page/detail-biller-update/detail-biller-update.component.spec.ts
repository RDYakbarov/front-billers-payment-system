import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailBillerUpdateComponent } from './detail-biller-update.component';

describe('DetailBillerUpdateComponent', () => {
  let component: DetailBillerUpdateComponent;
  let fixture: ComponentFixture<DetailBillerUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailBillerUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailBillerUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
