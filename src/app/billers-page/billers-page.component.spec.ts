import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillersPageComponent } from './billers-page.component';

describe('BillersPageComponent', () => {
  let component: BillersPageComponent;
  let fixture: ComponentFixture<BillersPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillersPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
