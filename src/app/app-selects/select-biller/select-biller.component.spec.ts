import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBillerComponent } from './select-biller.component';

describe('SelectBillerComponent', () => {
  let component: SelectBillerComponent;
  let fixture: ComponentFixture<SelectBillerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBillerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBillerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
