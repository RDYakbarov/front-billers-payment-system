import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-payments-detail',
  templateUrl: './payments-detail.component.html',
  styleUrls: ['./payments-detail.component.css']
})
export class PaymentsDetailComponent implements OnInit {
  customers: string[] = [
    'test',
    'test',
    'test',
    'test',
    'test',
  ];
  billers: string[] = [
    'test',
    'test',
    'test',
    'test',
    'test',
  ];
  myform: FormGroup;

  ngOnInit() {
    this.myform = new FormGroup({
      customer: new FormControl(),
      biller: new FormControl(),
      amount: new FormControl(),
      account: new FormControl()
    });
  }
}
